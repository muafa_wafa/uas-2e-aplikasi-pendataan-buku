package adinata.rohman.projectuas

import android.app.Activity
import android.content.Intent
import android.icu.text.SimpleDateFormat
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_databuku.*
import org.json.JSONObject
import java.util.*
import kotlin.collections.HashMap

class databuku : AppCompatActivity(), View.OnClickListener {

    lateinit var MediaHelper :MediaHelper
    var uri3 = "http://192.168.43.111/uas_andro/query.php"
    var image = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_databuku)
        MediaHelper = MediaHelper(this)

        simpan_button.setOnClickListener(this)
        foto.setOnClickListener(this)
    }




    fun query(mode : String){
        val request = object : StringRequest(
            Method.POST,uri3,
            Response.Listener { response ->
                val jsonobject  = JSONObject(response)
                val error = jsonobject.getString("kode")
                if (error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil ", Toast.LENGTH_LONG).show()

                }else{
                    Toast.makeText(this,"Operasi Gagal ", Toast.LENGTH_LONG).show()
                } },
            Response.ErrorListener { error ->
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                var nmFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(Date())+".jpg"
                when(mode){
                    "insert"->{
                        hm.put("mode","insert")
                        hm.put("judul",input_judul.text.toString())
                        hm.put("pengarang",input_pengarang.text.toString())
                        hm.put("image",image)
                        hm.put("file",nmFile)
                        hm.put("penerbit",input_penerbit.text.toString())
                        hm.put("kategori",input_kat.text.toString())
                        hm.put("deskribsi",input_des.text.toString())

                    }
                    "update"->{
                        hm.put("mode","insert")
                        hm.put("judul",input_judul.text.toString())
                        hm.put("pengarang",input_pengarang.text.toString())
                        hm.put("image",image)
                        hm.put("penerbit",input_penerbit.text.toString())
                        hm.put("kategori",input_kat.text.toString())
                        hm.put("deskribsi",input_des.text.toString())
                    }
                    "delete"->{
                        hm.put("mode","insert")
                        hm.put("judul",input_judul.text.toString())
                        hm.put("pengarang",input_pengarang.text.toString())
                        hm.put("image",image)
                        hm.put("penerbit",input_penerbit.text.toString())
                        hm.put("kategori",input_kat.text.toString())
                        hm.put("deskribsi",input_des.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){

           // if (requestCode == MediaHelperKamera.getRcCamera()){
             //   imStr = MediaHelperKamera.getBitmapToString(imageView2,Fileuri)
              //  namafile = MediaHelperKamera.getMyfile()
            //}
             if (requestCode == MediaHelper.getRcGalery()){
                image = MediaHelper.getBitmapToString(data!!.data,pot)
             }
        }
    }


    override fun onClick(v: View?) {
        when(v?.id){
            R.id.simpan_button->{
                    query("insert")
            }

            R.id.foto->{

                 val intent  = Intent()
                intent.setType("image/*")
                intent.setAction(Intent.ACTION_GET_CONTENT)
                 startActivityForResult(intent,MediaHelper.getRcGalery())


            }
        }
    }


}
