package muafa.sirojul.projectuas

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class AdapterBuku(val dataBuk: List<HashMap<String,String>>, val lihat: lihat) : RecyclerView.Adapter<AdapterBuku.HolderDataMhs> (){
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterBuku.HolderDataMhs {
        val v  = LayoutInflater.from(parent.context).inflate(R.layout.isi_buku,parent,false)
        return HolderDataMhs(v)
    }

    override fun getItemCount(): Int {
        return dataBuk.size
    }





    override fun onBindViewHolder(holder: AdapterBuku.HolderDataMhs, position: Int) {
        val data =dataBuk.get(position)
        holder.txnim.setText(data.get("judul_buku"))
        holder.txnama.setText(data.get("pengarang"))
        holder.txProd.setText(data.get("penerbit"))
        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(holder.photo)
        holder.tgl.setText(data.get("deskribsi"))
        holder.jenis_kel.setText(data.get("kategori"))

        holder.btedite.setOnClickListener{
            Toast.makeText(this.lihat , "Edit data ",Toast.LENGTH_SHORT).show()
        }
        holder.bthap.setOnClickListener {
            Toast.makeText(this.lihat, "Hapus data ", Toast.LENGTH_SHORT).show()
        }
    }

    class HolderDataMhs(v : View) :  RecyclerView.ViewHolder(v){
        val txnim = v.findViewById<TextView>(R.id.judul)
        val txnama = v.findViewById<TextView>(R.id.pengarangbuku)
        val txProd = v.findViewById<TextView>(R.id.Penerbitbuku)
        val photo = v.findViewById<ImageView>(R.id.ImgUpload)
        val tgl =v.findViewById<TextView>(R.id.deskripsibuku)
        val jenis_kel  = v.findViewById<TextView>(R.id.kategoribuku)
        val btedite = v.findViewById<Button>(R.id.btn_edit)
        val bthap = v.findViewById<Button>(R.id.btn_hapus)

    }


}