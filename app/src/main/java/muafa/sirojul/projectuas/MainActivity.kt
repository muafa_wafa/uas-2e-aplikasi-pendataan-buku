package muafa.sirojul.projectuas

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        datamhs.setOnClickListener(this)
        profil.setOnClickListener(this)


    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.datamhs->{
                val inten = Intent(this,databuku::class.java)
                startActivity(inten)
            }
            R.id.profil->{
                val inte = Intent(this , lihat::class.java)
                startActivity(inte)
            }

        }

    }
}
