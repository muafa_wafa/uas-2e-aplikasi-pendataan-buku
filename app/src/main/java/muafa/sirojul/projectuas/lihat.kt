package adinata.rohman.projectuas

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_lihat.*
import org.json.JSONArray

class lihat : AppCompatActivity() {

    var daftarmhs = mutableListOf<HashMap<String,String>>()
    var daftarprodi = mutableListOf<String>()
    //var imStr = ""
    lateinit var AdapterBuku : AdapterBuku
    lateinit var adapProd : ArrayAdapter<String>
//    lateinit var MediaHelper : MediaHelper
    var uri = "http://192.168.43.111/uas_andro/show_data.php"
   // var uri1 = "http://192.168.43.126/Kampus/show_prodi.php"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lihat)
        AdapterBuku = AdapterBuku(daftarmhs,this)
        Rcbuku.layoutManager = LinearLayoutManager(this)
        Rcbuku.adapter = AdapterBuku


    }

    override fun onStart() {
        super.onStart()
        ShowMhs()
    }



    fun ShowMhs(){
        val request = StringRequest(Request.Method.POST,uri, Response.Listener { response ->
            daftarmhs.clear()
            val jsonArray = JSONArray(response)
            for (x in 0..(jsonArray.length()-1)){
                val jsonObject = jsonArray.getJSONObject(x)
                var mhs = HashMap<String,String>()
                mhs.put("id_buku",jsonObject.getString("id_buku"))
                mhs.put("judul_buku",jsonObject.getString("judul_buku"))
                mhs.put("penerbit",jsonObject.getString("penerbit"))
                mhs.put("deskribsi",jsonObject.getString("deskribsi"))
                mhs.put("kategori",jsonObject.getString("kategori"))
                mhs.put("pengarang",jsonObject.getString("pengarang"))
                mhs.put("url",jsonObject.getString("url"))

                daftarmhs.add(mhs)
            }
            AdapterBuku.notifyDataSetChanged()
        },Response.ErrorListener { error ->
            Toast.makeText(this,"Kesalahan Saat Konfigurasi", Toast.LENGTH_SHORT).show()
        })
        val queue =  Volley.newRequestQueue(this)
        queue.add(request)
    }
}
